#include "../colvar/Colvar.h"
#include "../colvar/ActionRegister.h"
#include "../tools/Matrix.h"
#include "../tools/Vector.h"

#include <iostream>

namespace PLMD {
namespace colvar {

//+PLUMEDOC COLVAR ALPHA_FOLD
/*
\plumedfile
ALPHA_FOLD ...
label=prp
ATOMS=1,5
LAMBDA=1000
DISTANCES=0.110,0.111,0.112,0.113
LOGIT_MATRIX0=0,0.1,0.1,0
LOGIT_MATRIX1=0,0.4,0.4,0
LOGIT_MATRIX2=0,0.3,0.3,0
LOGIT_MATRIX3=0,0.2,0.2,0
... ALPHA_FOLD
\endplumedfile
*/
//+ENDPLUMEDOC


template <typename T>
class _3_Tensor {
  using storage_type = std::vector<std::vector<std::vector<T>>>;

  storage_type data;
  public:
    _3_Tensor() = default;

    _3_Tensor(size_t w, size_t h, size_t d)
    : data(storage_type(w, std::vector<std::vector<T>>(h, std::vector<T>(d)))) {}

    std::vector<std::vector<T>>& operator[](int i) { return data[i]; }
    const std::vector<std::vector<T>>& operator[](int i) const { return data[i]; }
};


class AlphaFold : public Colvar {
  using Matrix = PLMD::Matrix<double>;
  using Tensor = _3_Tensor<double>;

  std::vector<AtomNumber> atoms;
  Tensor logits;
  std::vector<double> dists;

  double lambda;

private:
  std::pair<double, double> interpolate(const std::vector<double>& logits, double dist);

public:
  explicit AlphaFold(const ActionOptions&);
// active methods:
  void calculate() override;
/// Register all the keywords for this action
  static void registerKeywords( Keywords& keys );
};

PLUMED_REGISTER_ACTION(AlphaFold,"ALPHA_FOLD")

AlphaFold::AlphaFold(const ActionOptions&ao)
  : PLUMED_COLVAR_INIT(ao)
  , lambda(1)
{
  addValueWithDerivatives(); setNotPeriodic();
  
  parseAtomList("ATOMS", atoms);
  parseVector("DISTANCES", dists);
  parse("LAMBDA", lambda);

  logits = Tensor(atoms.size(), atoms.size(), dists.size());
  std::vector<double> prob_vector(atoms.size() * atoms.size());
  
  for (size_t d = 0; d < dists.size(); ++d) {
    parseNumberedVector("LOGIT_MATRIX", d, prob_vector);
    
    /** Fill logit matrix for particular distance */
    for (size_t i = 0; i < atoms.size(); ++i) {
      for (size_t j = 0; j < atoms.size(); ++j) {
        logits[i][j][d] = prob_vector[i * atoms.size() + j];
      }
    }

    /** Check whether the matrix is symmetric */
    for (size_t i = 0; i < atoms.size(); ++i) {
      for (size_t j = 0; j < atoms.size(); ++j) {
        if (logits[i][j][d] != logits[j][i][d]) error("logits matrix is not symmetric");
      }
    }
  }

  requestAtoms(atoms);

  checkRead();
}

void AlphaFold::registerKeywords( Keywords& keys ) {
  Colvar::registerKeywords( keys );

  // componentsAreNotOptional(keys);

  keys.add("atoms","ATOMS","a list of atoms whose coordinates are used to compute the probability.");
  keys.add("compulsory", "DISTANCES", "a list of distances.");
  keys.add("numbered", "LOGIT_MATRIX", "a flattened matrix of probabilities for given distance from the DISTANCES list.");
  // keys.reset_style("LOGIT_MATRIX", "compulsory");
  keys.add("optional", "LAMBDA", "smoothness parameter of the property map.");
}

/**
 * Currently using Property map interpolation
 */
std::pair<double, double> AlphaFold::interpolate(const std::vector<double>& logits, double dist) {
  double sum = 0;
  double weighted_sum = 0;

  double d_sum = 0;
  double d_weighted_sum = 0;

  for (size_t d = 0; d < logits.size(); ++d) {
    double t = exp(-lambda * pow(dists[d] - dist, 2));
    
    sum += t;
    weighted_sum += t * logits[d];

    d_sum += t * (dists[d] - dist);
    d_weighted_sum += t * (dists[d] - dist) * logits[d];
  }

  d_sum *= 2 * lambda;
  d_weighted_sum *= 2 * lambda;

  std::cout << sum << " " << weighted_sum << " " << d_sum << " " << d_weighted_sum << std::endl;

  return { weighted_sum / sum, (d_weighted_sum * sum - weighted_sum * d_sum) / (sum * sum) };
}

// calculator
void AlphaFold::calculate() {
  std::vector<PLMD::Vector> positions = getPositions();

  Matrix real_dists(atoms.size(), atoms.size());
  for (size_t i = 0; i < atoms.size(); ++i) {
    for (size_t j = 0; j < atoms.size(); ++j) {
      real_dists(i, j) = real_dists(j, i) = delta(positions[i], positions[j]).modulo();
    }
  }

  Matrix gradient(atoms.size(), atoms.size());

  double prob_sum = 0;
  for(size_t i = 0; i < atoms.size(); ++i) {
    for(size_t j = i + 1; j < atoms.size(); ++j) {
      auto r = interpolate(logits[i][j], real_dists(i, j));
      prob_sum += r.first;
      gradient(i, j) = gradient(j, i) = r.second;
    }
  }

  for ( size_t i = 0; i < atoms.size(); ++i) {
    Vector derivatives;
    for ( size_t j = 0; j < atoms.size(); ++j) {
      if (i == j) continue;
      derivatives += delta(positions[j], positions[i]) * (gradient(i, j) / real_dists(i, j));
    }

    setAtomsDerivatives(i, derivatives);
  }
  setBoxDerivativesNoPbc();
  setValue(prob_sum);
}

} // end of namespace colvar
} // end of namespace PLMD



